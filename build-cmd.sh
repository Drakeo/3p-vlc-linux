#!/bin/bash

cd "$(dirname "$0")" 

# turn on verbose debugging output for parabuild logs.
set -x




if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

top="$(pwd)"
stage="$(pwd)/stage"

VLC_DIR="vlc-3.0.7"
BRANCH=$(echo $VLC_DIR|  cut -b 5-9)
BUILDDIR=$(pwd)/$VLC_DIR/contrib/build
VLC=$(pwd)/$VLC_DIR
BINDIR=$(pwd)$VLC_DIR/contrib/build/bin
#grab a fresh tar ball start a new build
if [ -d $VLC ]; then
  rm -rf $VLC
  rm -rf $stage/LICENSES  VERSION.txt bin include lib share autobuild-package.xml
  wget -c https://download.videolan.org/pub/videolan/vlc/$BRANCH/$VLC_DIR.tar.xz
else
  wget -c https://download.videolan.org/pub/videolan/vlc/$BRANCH/$VLC_DIR.tar.xz
fi
tar xvf $VLC_DIR.tar.xz

# used in VERSION.txt but common to all bit-widths and platforms
build=${AUTOBUILD_BUILD_ID:=0}


mkdir -p "$stage/LICENSES"

case "$AUTOBUILD_PLATFORM" in
    windows*)
    ;;

    "darwin")
    ;;

    "linux64")
# ubuntu sudo apt-get install subversion yasm cvs cmake ragel
# must be built as sudo or root do to linking and creating parts of the static build
# we do not configure --enable-static because contrib is static build vlc-307
    cd $VLC_DIR
    cd contrib
    mkdir -p build
    cd build
    export CFLAGS="-fPIC" CXXFLAGS="-fPIC"
# do not need goom it is for mixer graphics such as xxms 
# force lininking to libavcodec from ffmpeg static build in contrib
# disable disc here but build auto on the vlc build so it can be found

    ../bootstrap --disable-caca --disable-disc --disable-goom  --enable-ffmpeg
    make fetch
    make -j6 #build fetch
    make -j6  #build extra
    make -j6  #build ffmpeg
    cd $VLC
# set exit on error here do to contrib throws copying errors that have nothing to do with the build.
set -e 
./bootstrap
export CFLAGS="-fPIC" CXXFLAGS="-fPIC"
# build default vlc xcb conflict with viewer chromaprint not needed
    ./configure --prefix=$stage --enable-vlc --disable-xcb  --disable-chromaprint  

make -j6
make install


cd $top
        MAJOR=$(sed -n -E 's/.*define.*LIBVLC_VERSION_MAJOR.*\((.*)\)/\1/p' ${stage}/include/vlc/libvlc_version.h)
        MINOR=$(sed -n -E 's/.*define.*LIBVLC_VERSION_MINOR.*\((.*)\)/\1/p' ${stage}/include/vlc/libvlc_version.h)
        REVISON=$(sed -n -E 's/.*define.*LIBVLC_VERSION_REVISION.*\((.*)\)/\1/p' ${stage}/include/vlc/libvlc_version.h)
        
        echo "${MAJOR}.${MINOR}.${REVISON}" > ${stage}/VERSION.txt
        cp $VLC_DIR/COPYING "$stage/LICENSES/vlc.txt"
        ;;

    "linux")
    ;;
esac
